%%% reftex-default-bibliography: ("references.bib")
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

% Created 2021-11-18 Thu 14:24
\documentclass[fleqn,usenatbib]{mnras}

% Allow "Thomas van Noord" and "Simon de Laguarde" and alike to be sorted by "N" and "L" etc. in the bibliography.
% Write the name in the bibliography as "\VAN{Noord}{Van}{van} Noord, Thomas"
\DeclareRobustCommand{\VAN}[3]{#2}
\let\VANthebibliography\thebibliography
\def\thebibliography{\DeclareRobustCommand{\VAN}[3]{##3}\VANthebibliography}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{color}
\usepackage{listings}
\usepackage{natbib}
\usepackage{hypernat}

% Custom packages
\usepackage{/home/david/projects/latex_todo_upgrade/tex_todo}
\usepackage{/home/david/projects/latex_editing_colors/editing_colors}

% Custom commands
\newcommand{\binaryc}{\textsc{binary\_c}}
\newcommand{\binarycpython}{\textsc{binary\_c-python}}

% Title
\newcommand{\mytitle}{\binarycpython: A python-based stellar population synthesis tool and interface to \binaryc}

%%%%%%%%%%%%%%%%%%%%% 
% The list of authors, and the short list which is used in the headers.
% If you need two or more lines of authors, add an extra line using \newauthor
\author[D.D. Hendriks. et al.]{
  D. D. Hendriks $^{1}$\thanks{E-mail: \href{mailto:dh00601@surrey.ac.uk}{dh00601@surrey.ac.uk}\ (DDH)},
  R. G. Izzard $^{1}$
\\
% List of institutions
  $^{1}$Department of Physics, University of Surrey, Guildford, GU2 7XH, Surrey, UK\\
}

\title[]{\mytitle}
\hypersetup{
 pdfauthor={David Hendriks},
 pdftitle={\mytitle},
 pdfkeywords={},
 pdfsubject={},
 pdflang={English}
}
%%%
\begin{document}

\maketitle

\section{Summary}
\label{sec:summary}
% general astrophysics
%Calculating the evolution of populations of stars, and investigating how certain physical assumptions affect this evolution, requires methods to rapidly simulate stellar systems.

% Intro sentence
We present our package \href{https://ri0005.pages.surrey.ac.uk/binary_c-python/}{\binarycpython}, which is aimed to provide a convenient and easy-to-use interface to the \href{http://personal.ph.surrey.ac.uk/~ri0005/doc/binary_c/binary_c.html}{\binaryc}~\citep{izzardNewSyntheticModel2004, izzardPopulationNucleosynthesisSingle2006, izzardPopulationSynthesisBinary2009, izzardBinaryStarsGalactic2018} framework, allowing the user to rapidly evolve individual systems and populations of stars. \binarycpython\ is available on \href{https://pypi.org/project/binarycpython/}{Pip} and on \href{https://gitlab.com/binary_c/binary_c-python}{Gitlab}.

% A bit more about what we can do
The user can control output from \binaryc\ by providing \binarycpython\ with logging statements that are dynamically compiled and loaded into \binaryc. \binarycpython\ uses multiprocessing to utilise all the cores on a particular machine, and can run populations with HPC cluster workload managers like \textsc{HTCondor} and \textsc{Slurm}, allowing the user to run simulations on very large computing clusters. 

% Interfacing
\binarycpython\ is easily interfaced or integrated with other Python-based codes and libraries, e.g.\ sampling codes like \textsc{Emcee} or \textsc{Dynesty}, or the astrophysics oriented package \textsc{Astropy}~\citep{astropycollaborationAstropyCommunityPython2013,foreman-mackeyEmceeMCMCHammer2013,astropycollaborationAstropyProjectBuilding2018,speagleDynestyDynamicNested2020}. Moreover, it is possible to provide custom system-generating functions through our function hooks, allowing third-party packages to manage the properties of the stars in the populations and evolve them through \binarycpython. 

% Recent developments
Recent developments in \binaryc\ include standardised output datasets called \textit{ensembles}. \binarycpython\ easily processes these datasets and provides a suite of utility functions to handle them. Furthermore, \binaryc\ now includes the \textit{ensemble-manager} class, which makes use of the core functions and classes of \binarycpython\ to evolve a grid of stellar populations with varying input physics, allowing for large, automated parameter studies through a single interface.

% Documentation
We provide \href{https://ri0005.pages.surrey.ac.uk/binary\_c-python/index.html}{documentation} that is automatically generated based on docstrings and a suite of \textsc{Jupyter} \href{https://ri0005.pages.surrey.ac.uk/binary\_c-python/example\_notebooks.html}{notebooks}. These notebooks consist of technical tutorials on how to use \binarycpython, and use-case scenarios aimed at doing science. Much of \binarycpython\ is covered by unit tests to ensure reliability and correctness, and the test coverage is continually increased as the package is being improved. 

\section{Statement of need}
\label{sec:statement}
% Current climate
In the current scientific climate \textsc{Python} is ubiquitous, and while lower-level codes written in, e.g., \textsc{Fortran} or \textsc{C} are still widely used, much of the newer software is written in \textsc{Python}, either entirely or as a wrapper around other codes and libraries. Education in programming also often includes \textsc{Python} courses because of its ease of use and its flexibility. Moreover, \textsc{Python} has a large community with many resources and tutorials. 
% Why
We have created \binarycpython\ to allow students and scientists alike to explore current scientific issues while enjoying the familiar syntax, and at the same time make use of the plentiful scientific and astrophysical packages like \textsc{Numpy}, \textsc{Scipy}, \textsc{Pandas}, \textsc{Astropy}\ and platforms like \textsc{Jupyter}. 

% Earlier versions
Earlier versions of \binarycpython\ were written in Perl, where much of the logic and structure were developed and debugged. This made porting to \textsc{Python} relatively easy.

\section{Projects that use \binarycpython}
\label{sec:projects}
% Intro to projects
\binarycpython\ has already been used in a variety of situations, ranging from pure research to educational purposes, as well as in outreach events.
% Educational
In the summer of 2021 we used \binarycpython\ as the basis for the interactive classes on stellar ecosystems during the \href{https://www2.mpia-hd.mpg.de/imprs-hd/SummerSchools/2021/}{International Max-Planck Research School summer school 2021 in Heidelberg}, where students were introduced to the topic of population synthesis and were able to use our notebooks to perform their own calculations. 
%% Past Research
%
\binarycpython\ has been used in \citet{mirouh_etal22}, where improvements to tidal interactions between stars were implemented, and initial birth parameter distributions were varied to match to observed binary systems in star clusters.
%
A Master’s thesis project, aimed at finding the birth system parameters of the V106 stellar system, comparing observations to results of \binaryc\ and calculating the maximum likelihood with Bayesian inference through Markov chain Monte Carlo sampling. The project made use of \binarycpython\ and the \textsc{Emcee} package.

% 
Currently \binarycpython\ is used in several ongoing projects that study the effect of birth distributions on the occurrence of carbon-enhanced metal-poor (CEMP) stars, the occurrence and properties of accretion disks in main-sequence stars and the predicted observable black hole distribution by combining star formation and metallicity distributions with the output of \binaryc. Moreover, we use the \textit{ensemble} output structure to generate datasets for galactic chemical evolution on cosmological timescales, where we rely heavily on the utilities of \binarycpython.

\section{Acknowledgements}
\label{sec:orgf6f5520}
We acknowledge the helpful discussions and early testing efforts from M. Delorme, G. Mirouh, and D. Tracey, and the early work of J. Andrews which inspired our Python-C interface code. DDH thanks the UKRI/UoS for the funding grant H120341A. RGI thanks STFC for funding grants \href{https://gtr.ukri.org/projects?ref=ST%2FR000603%2F1}{ST/R000603/1} and \href{https://gtr.ukri.org/projects?ref=ST/L003910/2}{ST/L003910/2}.

%%%%%%%%%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%
% The best way to enter references is to use BibTeX:
\bibliographystyle{mnras-custom.bst}
\bibliography{references.bib} % if your bibtex file is called example.bib

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
