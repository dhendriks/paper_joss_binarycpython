---
title: '[binary\_c-python]{.smallcaps}: A python-based stellar population synthesis tool and interface to [binary\_c]{.smallcaps}'
tags:
  - Python
  - astronomy
authors:
  - name: D. D. Hendriks[^1]
    orcid: 0000-0002-8717-6046
    affiliation: 1 
  - name: R. G. Izzard
    orcid: 0000-0003-0378-4843
    affiliation: 1
affiliations:
 - name: Department of Physics, University of Surrey, Guildford, GU2 7XH, Surrey, UK
   index: 1
date: 18 June 2022
bibliography: paper.bib
---