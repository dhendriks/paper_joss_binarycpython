---
author:
- |
    D. D. Hendriks $^{1}$[^1], R. G. Izzard $^{1}$\
    $^{1}$Department of Physics, University of Surrey, Guildford, GU2 7XH,
    Surrey, UK\
bibliography:
- 'references.bib'
title: |
    [binary\_c-python]{.smallcaps}: A python-based stellar population
    synthesis tool and interface to [binary\_c]{.smallcaps}
---

Summary {#sec:summary}
=======

We present our package
[[binary\_c-python]{.smallcaps}](https://ri0005.pages.surrey.ac.uk/binary_c-python/),
which is aimed to provide a convenient and easy-to-use interface to the
[[binary\_c]{.smallcaps}](http://personal.ph.surrey.ac.uk/~ri0005/doc/binary_c/binary_c.html) [@izzardNewSyntheticModel2004; @izzardPopulationNucleosynthesisSingle2006; @izzardPopulationSynthesisBinary2009; @izzardBinaryStarsGalactic2018]
framework, allowing the user to rapidly evolve individual systems and
populations of stars. [binary\_c-python]{.smallcaps} is available on
[Pip](https://pypi.org/project/binarycpython/) and on
[Gitlab](https://gitlab.com/binary_c/binary_c-python).

The user can control output from [binary\_c]{.smallcaps} by providing
[binary\_c-python]{.smallcaps} with logging statements that are
dynamically compiled and loaded into [binary\_c]{.smallcaps}.
[binary\_c-python]{.smallcaps} uses multiprocessing to utilise all the
cores on a particular machine, and can run populations with HPC cluster
workload managers like [HTCondor]{.smallcaps} and [Slurm]{.smallcaps},
allowing the user to run simulations on very large computing clusters.

[binary\_c-python]{.smallcaps} is easily interfaced or integrated with
other Python-based codes and libraries, e.g. sampling codes like
[Emcee]{.smallcaps} or [Dynesty]{.smallcaps}, or the astrophysics
oriented package
[Astropy]{.smallcaps} [@astropycollaborationAstropyCommunityPython2013; @foreman-mackeyEmceeMCMCHammer2013; @astropycollaborationAstropyProjectBuilding2018; @speagleDynestyDynamicNested2020].
Moreover, it is possible to provide custom system-generating functions
through our function hooks, allowing third-party packages to manage the
properties of the stars in the populations and evolve them through
[binary\_c-python]{.smallcaps}.

Recent developments in [binary\_c]{.smallcaps} include standardised
output datasets called *ensembles*.
[binary\_c-python]{.smallcaps} easily processes these datasets and
provides a suite of utility functions to handle them. Furthermore,
[binary\_c]{.smallcaps} now includes the *ensemble-manager* class, which
makes use of the core functions and classes of
[binary\_c-python]{.smallcaps} to evolve a grid of stellar populations
with varying input physics, allowing for large, automated parameter
studies through a single interface.

We provide
[documentation](https://ri0005.pages.surrey.ac.uk/binary_c-python/index.html)
that is automatically generated based on docstrings and a suite of
[Jupyter]{.smallcaps}
[notebooks](https://ri0005.pages.surrey.ac.uk/binary_c-python/example_notebooks.html).
These notebooks consist of technical tutorials on how to use
[binary\_c-python]{.smallcaps}, and use-case scenarios aimed at doing
science. Much of [binary\_c-python]{.smallcaps} is covered by unit tests
to ensure reliability and correctness, and the test coverage is
continually increased as the package is being improved.

Statement of need {#sec:statement}
=================

In the current scientific climate [Python]{.smallcaps} is ubiquitous,
and while lower-level codes written in, e.g., [Fortran]{.smallcaps} or
[C]{.smallcaps} are still widely used, much of the newer software is
written in [Python]{.smallcaps}, either entirely or as a wrapper around
other codes and libraries. Education in programming also often includes
[Python]{.smallcaps} courses because of its ease of use and its
flexibility. Moreover, [Python]{.smallcaps} has a large community with
many resources and tutorials. We have created
[binary\_c-python]{.smallcaps} to allow students and scientists alike to
explore current scientific issues while enjoying the familiar syntax,
and at the same time make use of the plentiful scientific and
astrophysical packages like [Numpy]{.smallcaps}, [Scipy]{.smallcaps},
[Pandas]{.smallcaps}, [Astropy]{.smallcaps} and platforms like
[Jupyter]{.smallcaps}.

Earlier versions of [binary\_c-python]{.smallcaps} were written in Perl,
where much of the logic and structure were developed and debugged. This
made porting to [Python]{.smallcaps} relatively easy.

Projects that use [binary\_c-python]{.smallcaps} {#sec:projects}
================================================

[binary\_c-python]{.smallcaps} has already been used in a variety of
situations, ranging from pure research to educational purposes, as well
as in outreach events. In the summer of 2021 we used
[binary\_c-python]{.smallcaps} as the basis for the interactive classes
on stellar ecosystems during the [International Max-Planck Research
School summer school 2021 in
Heidelberg](https://www2.mpia-hd.mpg.de/imprs-hd/SummerSchools/2021/),
where students were introduced to the topic of population synthesis and
were able to use our notebooks to perform their own calculations.
[binary\_c-python]{.smallcaps} has been used in @mirouh_etal22, where
improvements to tidal interactions between stars were implemented, and
initial birth parameter distributions were varied to match to observed
binary systems in star clusters. A Master's thesis project, aimed at
finding the birth system parameters of the V106 stellar system,
comparing observations to results of [binary\_c]{.smallcaps} and
calculating the maximum likelihood with Bayesian inference through
Markov chain Monte Carlo sampling. The project made use of
[binary\_c-python]{.smallcaps} and the [Emcee]{.smallcaps} package.

Currently [binary\_c-python]{.smallcaps} is used in several ongoing
projects that study the effect of birth distributions on the occurrence
of carbon-enhanced metal-poor (CEMP) stars, the occurrence and
properties of accretion disks in main-sequence stars and the predicted
observable black hole distribution by combining star formation and
metallicity distributions with the output of [binary\_c]{.smallcaps}.
Moreover, we use the *ensemble* output structure to generate datasets
for galactic chemical evolution on cosmological timescales, where we
rely heavily on the utilities of [binary\_c-python]{.smallcaps}.

Acknowledgements {#sec:orgf6f5520}
================

We acknowledge the helpful discussions and early testing efforts from M.
Delorme, G. Mirouh, and D. Tracey, and the early work of J. Andrews
which inspired our Python-C interface code. DDH thanks the UKRI/UoS for
the funding grant H120341A. RGI thanks STFC for funding grants
[ST/R000603/1](https://gtr.ukri.org/projects?ref=ST%2FR000603%2F1) and
[ST/L003910/2](https://gtr.ukri.org/projects?ref=ST/L003910/2).

[^1]: E-mail: <dh00601@surrey.ac.uk> (DDH)
