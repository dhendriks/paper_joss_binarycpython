#+AUTHOR: David Hendriks
#+EMAIL: davidhendriks93@gmail.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:2 ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tasks:t tex:t timestamp:t toc:t todo:t |:t
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+BIBLIOGRAPHY:
#+OPTIONS: \n:nil tags:nil
#+LATEX: \setlength\parindent{0pt}
#+TITLE: 
#+DATE: 

* Requirements for submission:
** DONE Requirements for the project
*** DONE The software must be open source as per the OSI definition.
  
  Yes.  
*** DONE The software must have an obvious research application.

  Yes: main tool used to operate binary-c and generate populations of stars through various mtehods  
*** DONE You must be a major contributor to the software you are submitting, and have a GitHub account to participate in the review process.

  Yes. https://github.com/ddhendriks
*** DONE Your paper must not focus on new research results accomplished with the software.

  It does not.
*** DONE Your paper (paper.md and BibTeX files, plus any figures) must be hosted in a Git-based repository together with your software (although they may be in a short-lived branch which is never merged with the default).
*** DONE Be stored in a repository that can be cloned without registration.

  Yes, it does. https://gitlab.com/binary_c/binary_c-python
*** DONE Be stored in a repository that is browsable online without registration.

  Yes, it does. https://gitlab.com/binary_c/binary_c-python
*** DONE Have an issue tracker that is readable without registration.

  Yes, it does. https://gitlab.com/binary_c/binary_c-python/-/issues
*** DONE Permit individuals to create issues/file tickets against your repository.

  Yes. It does. https://gitlab.com/binary_c/binary_c-python/-/issues
*** DONE Your software should be a significant contribution to the available open source software that either enables some new research challenges to be addressed or makes addressing research challenges significantly better (e.g., faster, easier, simpler).
*** DONE should represent not less than three months of work for an individual.

  It represents work done throughout my PhD
    
** DONE submission workflow
*** DONE Make your software available in an open repository (GitHub, Bitbucket, etc.) and include an OSI approved open source license.

  Available on https://gitlab.com/binary_c/binary_c-python licenced under GPL3

*** DONE Make sure that the software complies with the JOSS review criteria. In particular, your software should be full-featured, well-documented, and contain procedures (such as automated tests) for checking correctness.

*** DONE Write a short paper in Markdown format using paper.md as file name, including a title, summary, author names, affiliations, and key references. See our example paper to follow the correct format.

  Working on it

*** DONE (Optional) create a metadata file describing your software and include it in your repository. We provide a script that automates the generation of this metadata.

  Yes, is included in https://gitlab.surrey.ac.uk/ri0005/binary_c-python/-/blob/papers/JOSS_release/papers/joss_paper/codemeta.json
** DONE Paper should include
*** DONE A list of the authors of the software and their affiliations, using the correct format (see the example below).
*** DONE A summary describing the high-level functionality and purpose of the software for a diverse, non-specialist audience.
*** DONE A Statement of Need section that clearly illustrates the research purpose of the software.
*** DONE A list of key references, including to other software addressing related needs. Note that the references should include full names of venues, e.g., journals and conferences, not abbreviations only understood in the context of a specific discipline.
*** DONE Mention (if applicable) a representative set of past or ongoing research projects using the software and recent scholarly publications enabled by it.
*** DONE Acknowledgement of any financial support.
* Review checklist
https://joss.readthedocs.io/en/latest/review_checklist.html
** General checks
*** DONE Repository: Is the source code for this software available at the repository url?
*** DONE License: Does the repository contain a plain-text LICENSE file with the contents of an OSI approved software license?
*** DONE Contribution and authorship: Has the submitting author made major contributions to the software? Does the full list of paper authors seem appropriate and complete?
** DONE Functionality
*** DONE Installation: Does installation proceed as outlined in the documentation?
*** ANSWERED Functionality: Have the functional claims of the software been confirmed?
*** DONE Performance: If there are any performance claims of the software, have they been confirmed? (If there are no claims, please check off this item.)
** DONE Documentation
*** DONE A statement of need: Do the authors clearly state what problems the software is designed to solve and who the target audience is?
*** DONE Installation instructions: Is there a clearly-stated list of dependencies? Ideally these should be handled with an automated package management solution.
*** DONE Example usage: Do the authors include examples of how to use the software (ideally to solve real-world analysis problems).
*** DONE Functionality documentation: Is the core functionality of the software documented to a satisfactory level (e.g., API method documentation)?
*** DONE Automated tests: Are there automated tests or manual steps described so that the functionality of the software can be verified?
*** DONE Community guidelines: Are there clear guidelines for third parties wishing to 1) Contribute to the software 2) Report issues or problems with the software 3) Seek support
** TODO Software paper
*** DONE Summary: Has a clear description of the high-level functionality and purpose of the software for a diverse, non-specialist audience been provided?
*** DONE A statement of need: Does the paper have a section titled ‘Statement of Need’ that clearly states what problems the software is designed to solve and who the target audience is?
*** TODO State of the field: Do the authors describe how this software compares to other commonly-used packages?

Well, not sure if i want to do that. If they ask about it then i will add more text i think

*** TODO Quality of writing: Is the paper well written (i.e., it does not require editing for structure, language, or writing quality)?

That is to the reviewer

*** DONE References: Is the list of references complete, and is everything cited appropriately that should be cited (e.g., papers, datasets, software)? Do references in the text use the proper citation syntax?
