** Requirements for submission:
- [X] The software must be open source as per the OSI definition.
- [ ] The software must have an obvious research application.
- [X]  You must be a major contributor to the software you are submitting, and have a GitHub account to participate in the review process.
- [X] Your paper must not focus on new research results accomplished with the software.
- [ ] Your paper (paper.md and BibTeX files, plus any figures) must be hosted in a Git-based repository together with your software (although they may be in a short-lived branch which is never merged with the default).
- [ ] Be stored in a repository that can be cloned without registration.
- [ ] Be stored in a repository that is browsable online without registration.
- [X] Have an issue tracker that is readable without registration.
- [X] Permit individuals to create issues/file tickets against your repository.
- [ ] Your software should be a significant contribution to the available open source software that either enables some new research challenges to be addressed or makes addressing research challenges significantly better (e.g., faster, easier, simpler).
- [X] should represent not less than three months of work for an individual.

submission workflow
- [ ] Make your software available in an open repository (GitHub, Bitbucket, etc.) and include an OSI approved open source license.
- [ ] Make sure that the software complies with the JOSS review criteria. In particular, your software should be full-featured, well-documented, and contain procedures (such as automated tests) for checking correctness.
- [ ] Write a short paper in Markdown format using paper.md as file name, including a title, summary, author names, affiliations, and key references. See our example paper to follow the correct format.
- [ ] (Optional) create a metadata file describing your software and include it in your repository. We provide a script that automates the generation of this metadata.

Paper should include
- [ ] A list of the authors of the software and their affiliations, using the correct format (see the example below).
- [ ] A summary describing the high-level functionality and purpose of the software for a diverse, non-specialist audience.
- [ ] A Statement of Need section that clearly illustrates the research purpose of the software.
- [ ] A list of key references, including to other software addressing related needs. Note that the references should include full names of venues, e.g., journals and conferences, not abbreviations only understood in the context of a specific discipline.
- [X] Mention (if applicable) a representative set of past or ongoing research projects using the software and recent scholarly publications enabled by it.
- [ ] Acknowledgement of any financial support.

