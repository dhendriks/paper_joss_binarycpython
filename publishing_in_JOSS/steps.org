* Steps for publishing in JOSS

https://joss.readthedocs.io/en/latest/review_criteria.html


1) make sure we fulfill the submission requirements: https://joss.readthedocs.io/en/latest/submitting.html#submission-requirements
2) Follow sumbission flow: https://joss.readthedocs.io/en/latest/submitting.html#typical-paper-submission-flow
3) Check that your paper compiles: https://joss.readthedocs.io/en/latest/submitting.html#checking-that-your-paper-compiles
4) Submit the paper project by filling in a form and waiting
   https://joss.readthedocs.io/en/latest/submitting.html#submitting-your-paper
