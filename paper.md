---
title: '`binary_c-python`: A python-based stellar population synthesis tool and interface to `binary_c`'
tags:
  - Python
  - astronomy
authors:
  - name: D. D. Hendriks[^1]
    orcid: 0000-0002-8717-6046
    affiliation: 1
  - name: R. G. Izzard
    orcid: 0000-0003-0378-4843
    affiliation: 1
affiliations:
 - name: Department of Physics, University of Surrey, Guildford, GU2 7XH, Surrey, UK
   index: 1
date: 21 January 2023
bibliography: paper.bib
---

# Summary

We present our package [`binary_c-python`](https://binary_c.gitlab.io/binary_c-python/), which is aimed to provide a convenient and easy-to-use interface to the [`binary_c`](https://binary_c.gitlab.io/binary_c) [@izzardNewSyntheticModel2004;@izzardPopulationNucleosynthesisSingle2006;@izzardPopulationSynthesisBinary2009;@izzardBinaryStarsGalactic2018] framework, allowing the user to rapidly evolve individual systems and populations of stars. `binary_c-python` is available on [`Pip`](https://pypi.org/project/binarycpython/) and on [`Gitlab`](https://binary_c.gitlab.io/binary_c-python/).

The user can control output from `binary_c` by providing `binary_c-python` with logging statements that are dynamically compiled and loaded into `binary_c`. `binary_c-python` uses multiprocessing to utilise all the cores on a particular machine, and can run populations with HPC cluster workload managers like `HTCondor` and `Slurm`, allowing the user to run simulations on very large computing clusters.

`binary_c-python` is easily interfaced or integrated with other Python-based codes and libraries, e.g. sampling codes like `Emcee` [@foreman-mackeyEmceeMCMCHammer2013] or `Dynesty` [@speagleDynestyDynamicNested2020], or the astrophysics oriented package `Astropy` [@astropycollaborationAstropyCommunityPython2013; @astropycollaborationAstropyProjectBuilding2018]. Moreover, it is possible to provide custom system-generating functions through our function hooks, allowing third-party packages to manage the properties of the stars in the populations and evolve them through `binary_c-python`.

Recent developments in `binary_c` include standardised output datasets called *ensembles*. `binary_c-python` easily processes these datasets and provides a suite of utility functions to handle them. Furthermore, `binary_c` now includes the *ensemble-manager* class, which makes use of the core functions and classes of `binary_c-python` to evolve a grid of stellar populations with varying input physics, allowing for large, automated parameter studies through a single interface.

We provide [documentation](https://binary_c.gitlab.io/binary_c-python/readme_link.html) that is automatically generated based on docstrings and a suite of `Jupyter` [notebooks](https://binary_c.gitlab.io/binary_c-python/example_notebooks.html). These notebooks consist of technical tutorials on how to use `binary_c-python`, and use-case scenarios aimed at doing science. Much of `binary_c-python` is covered by unit tests to ensure reliability and correctness, and the test coverage is continually increased as the package is being improved.

# Statement of need

In the current scientific climate `Python` is ubiquitous, and while lower-level codes written in, e.g., `Fortran` or `C` are still widely used, much of the newer software is written in `Python`, either entirely or as a wrapper around other codes and libraries. Education in programming also often includes `Python` courses because of its ease of use and its flexibility. Moreover, `Python` has a large community with many resources and tutorials. We have created `binary_c-python` to allow students and scientists alike to explore current scientific issues while enjoying the familiar syntax, and at the same time make use of the plentiful scientific and astrophysical packages like `Numpy`, `Scipy`, `Pandas`, `Astropy` and platforms like `Jupyter`.

Earlier versions of `binary_c-python` were written in Perl, where much of the logic and structure were developed and debugged. This made porting to `Python` relatively easy.

# Projects that use `binary_c-python`

`binary_c-python` has already been used in a variety of situations, ranging from pure research to educational purposes, as well as in outreach events. In the summer of 2021 we used `binary_c-python` as the basis for the interactive classes on stellar ecosystems during the [International Max-Planck Research School summer school 2021 in Heidelberg](https://www2.mpia-hd.mpg.de/imprs-hd/SummerSchools/2021/), where students were introduced to the topic of population synthesis and were able to use our notebooks to perform their own calculations. `binary_c-python` has been used in @mirouh_etal22, where improvements to tidal interactions between stars were implemented, and initial birth parameter distributions were varied to match to observed binary systems in star clusters. A Master's thesis project, aimed at finding the birth system parameters of the V106 stellar system, comparing observations to results of `binary_c` and calculating the maximum likelihood with Bayesian inference through Markov chain Monte Carlo sampling. The project made use of `binary_c-python` and the `Emcee` package.

Currently `binary_c-python` is used in several ongoing projects that study the effect of birth distributions on the occurrence of carbon-enhanced metal-poor (CEMP) stars, the occurrence and properties of accretion disks in main-sequence stars and the predicted observable black hole distribution by combining star formation and metallicity distributions with the output of `binary_c`. Moreover, we use the *ensemble* output structure to generate datasets for galactic chemical evolution on cosmological timescales, where we rely heavily on the utilities of `binary_c-python`.

# Acknowledgements

We acknowledge the helpful discussions and early testing efforts from M. Delorme, G. Mirouh, and D. Tracey, and the early work of J. Andrews which inspired our Python-C interface code. DDH thanks the UKRI/UoS for the funding grant H120341A. RGI thanks STFC for funding grants [ST/R000603/1](https://gtr.ukri.org/projects?ref=ST%2FR000603%2F1) and [ST/L003910/2](https://gtr.ukri.org/projects?ref=ST/L003910/2).

# References

[^1]: E-mail: <davidhendriks93@gmail.com> (DDH)
