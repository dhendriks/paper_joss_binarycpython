---
title: '`binary_c-python`: A Python-based stellar population synthesis tool and interface to `binary_c`'
tags:
  - Python
  - astronomy
authors:
  - name: D. D. Hendriks[^1]
    orcid: 0000-0002-8717-6046
    affiliation: 1
  - name: R. G. Izzard
    orcid: 0000-0003-0378-4843
    affiliation: 1
affiliations:
 - name: Department of Physics, University of Surrey, Guildford, GU2 7XH, Surrey, UK
   index: 1
date: 11 April 2023
bibliography: paper.bib
---

# Summary

We present our package [`binary_c-python`](https://binary_c.gitlab.io/binary_c-python/), which is aimed to provide a convenient and easy-to-use interface to the [`binary_c`](https://binary_c.gitlab.io/binary_c) [@izzardNewSyntheticModel2004;@izzardPopulationNucleosynthesisSingle2006;@izzardPopulationSynthesisBinary2009;@izzardBinaryStarsGalactic2018] framework, allowing the user to rapidly evolve individual systems and populations of stars. `binary_c-python` is available on [`Pip`](https://pypi.org/project/binarycpython/) and on [`GitLab`](https://binary_c.gitlab.io/binary_c-python/).

`binary_c-python` contains many useful features that allow controlling and processing the output of `binary_c`. The user can control output from `binary_c` by providing `binary_c-python` with logging statements that are dynamically compiled and loaded into `binary_c`. Moreover, we have recently added standardised output of events like Roche-lobe overflow or double compact-object formation to `binary_c`, and automatic parsing and managing of that output in `binary_c-python`. `binary_c-python` uses multiprocessing to utilise all the cores on a particular machine, and can run populations with HPC cluster workload managers like `HTCondor` and `Slurm`, allowing the user to run simulations on large computing clusters.

Recent developments in `binary_c` include standardised output datasets called *ensembles*, which contain nested histograms of binned data like supernovae rates or chemical yields. `binary_c-python` easily processes these datasets and provides a suite of utility functions to handle them. Furthermore, `binary_c` now includes the *ensemble-manager* class, which makes use of the core functions and classes of `binary_c-python` to evolve a grid of stellar populations with varying input physics, allowing for large, automated parameter studies through a single interface.

`binary_c-python` is easily integrated with other Python-based codes and libraries, e.g. sampling codes like `Emcee` [@foreman-mackeyEmceeMCMCHammer2013] or `Dynesty` [@speagleDynestyDynamicNested2020], or the astrophysics oriented package `Astropy` [@astropycollaborationAstropyCommunityPython2013; @astropycollaborationAstropyProjectBuilding2018]. Moreover, it is possible to provide custom system-generating functions through our function hooks, allowing third-party packages to manage the properties of the stars in the populations and evolve them through `binary_c-python`.

We provide [documentation](https://binary_c.gitlab.io/binary_c-python/readme_link.html) that is automatically generated based on *docstrings* and a suite of `Jupyter` [notebooks](https://binary_c.gitlab.io/binary_c-python/example_notebooks.html). These notebooks consist of technical tutorials on how to use `binary_c-python` and use-case scenarios aimed at doing science. Much of `binary_c-python` is covered by unit tests to ensure reliability and correctness, and the test coverage is continually increased as the package is improved.

# Statement of need

In the current scientific climate `Python`, is ubiquitous. While lower level code written in, e.g., `Fortran` or `C` is still widely used, much of the newer software is written in `Python`, either entirely or as a wrapper around other code and libraries. Education in programming also often includes `Python` courses because of its ease of use and its flexibility. Moreover, `Python` has a large community with many resources and tutorials. We have created `binary_c-python` to allow students and scientists alike to explore current scientific issues while enjoying the familiar syntax, and at the same time make use of the plentiful scientific and astrophysical packages like `Numpy`, `Scipy`, `Pandas`, `Astropy` and platforms like `Jupyter`.

Over time, many (binary) stellar population synthesis codes have been created. Each one usually has a slightly different focus, like gravitational waves or spectral synthesis. Recent ones are `BPASS/HOKI`[@hoki], `SeBa`[@seba], `MOBSE`[@MOBSE], `COSMIC`[@COSMIC] and `COMPAS`[@COMPAS]. Most of these have `Python` interfaces, which further highlights the need to develop and release a modern `Python` interface to `binary_c`, i.e. `binary_c-python`.

The previous interface to `binary_c`, `binary_grid` was written in `Perl`, where much of the logic and structure was developed and debugged. While much of the code base of `binary_c-python` has changed significantly from its `Perl` predecessor, the initial porting to `Python` and the development of `binary_c-python` greatly benefitted from the existence of this earlier interface.

# Projects that use `binary_c-python`

`binary_c-python` has already been used in a variety of situations, ranging from pure research to educational purposes, as well as in outreach events. In the summer of 2021 we used `binary_c-python` as the basis for the interactive classes on stellar ecosystems during the [International Max-Planck Research School summer school 2021 in Heidelberg](https://www2.mpia-hd.mpg.de/imprs-hd/SummerSchools/2021/). Students were introduced to the topic of population synthesis and were able to use our notebooks to perform their own calculations. `binary_c-python` has been used in @mirouh_etal22, implementing improvements to tidal interactions between stars and varying initial birth parameter distributions to match to observed binary systems in star clusters. The `binary_c-python` and `Emcee` packages were used in a Master's thesis project to find the birth system parameters of the V106 stellar system, compare observations to results of `binary_c`, and perform uncertainty with Bayesian uncertainty inference through Markov chain Monte Carlo sampling.

Currently, `binary_c-python` is used in several ongoing projects. These include a study on the effect of birth distributions on the occurrence of carbon-enhanced metal-poor (CEMP) stars, the abundance and properties of accretion disks in main-sequence stars, and the predicted observable black hole distribution by combining star formation and metallicity distributions with the output of `binary_c`. We also use the *ensemble* output structure to generate datasets for galactochemical evolution over cosmological timescales, where we rely heavily on the utilities of `binary_c-python`.

# Acknowledgements

We acknowledge the helpful discussions and early testing efforts from M. Delorme, G. Mirouh, M. Renzo, and D. Tracey, and the early work of J. Andrews which inspired our Python-C interface code. DDH thanks the UKRI/UoS for the funding grant H120341A. RGI thanks STFC for funding grants [ST/R000603/1](https://gtr.ukri.org/projects?ref=ST%2FR000603%2F1) and [ST/L003910/2](https://gtr.ukri.org/projects?ref=ST/L003910/2).

# References

[^1]: E-mail: <davidhendriks93@gmail.com> (DDH)
